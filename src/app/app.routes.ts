import { Routes } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { TitleComponent } from './title/title.component';
import { DirectiveComponent } from './directive/directive.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UsersComponent } from './users/users.component';
import { IndexComponent as IndexUserComponent } from './users/index/index.component';
import { NewComponent as NewUserComponent } from './users/new/new.component';
import { EditComponent as EditUserComponent } from './users/edit/edit.component';

export const routes: Routes = [{
  path: '', component: TitleComponent
},{
  path: 'todo-utility', component: TodoComponent
},{
  path: 'directive', component: DirectiveComponent
},{
  path: 'users', component: UsersComponent, children: [{
      path: '', component: IndexUserComponent
    },{
      path: 'new', component: NewUserComponent
    },{
      path: ':id/edit', component: EditUserComponent
  }]
},{
  path: '**', component: NotFoundComponent
}];
