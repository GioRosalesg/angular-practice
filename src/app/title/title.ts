export interface Image {
  src: string;
  alt: string;
};

export interface TitleForm {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export type FormData = "firstName" | "lastName" | "email" | "password"