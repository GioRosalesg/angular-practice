import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
//Interfaces
import { Image, TitleForm, FormData } from './title';



@Component({
  selector: 'app-title',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './title.component.html',
  styleUrl: './title.component.scss'
})
export class TitleComponent {
  name: string = "Giovanni";
  image: Image = {
    src: "https://placehold.co/600x400",
    alt: "Placeholder image",
  };
  formData: TitleForm = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  };
  formData2: TitleForm = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  };
  handleOnChange(e: Event) {
    const target = e.target as HTMLInputElement;

    this.formData2[target?.name as FormData] = target.value;
    return;
  }
  handleSubmit(e: Event): void {
    e.preventDefault();
    console.log(this.formData);
    return;
  }
  handleSubmit2(e: Event): void {
    e.preventDefault();
    console.log(this.formData2);
    return;
  }
}
