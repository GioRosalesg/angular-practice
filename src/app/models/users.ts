export declare type UID = string;
export interface User {
  uid: UID;
  firstName: string;
  lastName: string;
}
export interface UserError {
  error: boolean;
  message: string;
}
const STRING_MIN_CHARACTERS = 4;
const STRING_MIN_CHARACTERS_ERROR_MESSAGE = `User must contain more than ${STRING_MIN_CHARACTERS} characters`;

export const Users: User[] = [
  {
    uid: "1234asd",
    firstName: "Jazz",
    lastName: "Urciel"
  },
  {
    uid: "1234ssasd",
    firstName: "Lesly",
    lastName: "Burgara"
  },
  {
    uid: "1234asdd",
    firstName: "Dinorah",
    lastName: "Cortina"
  },
  {
    uid: "1234ad",
    firstName: "Giovanni",
    lastName: "Rosales"
  },
  {
    uid: "1234asdp",
    firstName: "Samuel",
    lastName: "Villasenor"
  }
];

const validateRequiredString = (string: string) => {
  return string.length >= STRING_MIN_CHARACTERS;
};
const generateUserUid = () => crypto.randomUUID();
const validateUser = (user: User): UserError => {
  const validUser: boolean = validateRequiredString(user.firstName) && validateRequiredString(user.lastName);
  const error: UserError = {
    error: !validUser,
    message: !validUser ? STRING_MIN_CHARACTERS_ERROR_MESSAGE : ''
  };

  return error;
};
const getUserIndex = (uid: UID) => {
  return Users.findIndex((User) => {
    User.uid === uid;
  });
}
export const fetchUser = async (userId: UID) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(Users.find((user) => user.uid === userId));
    }, 2000);
  });
}
export const createUser = (user: User) => {
  const userValidation = validateUser(user);

  if (userValidation.error) {
    return userValidation;
  }
  const newUid = generateUserUid();
  user.uid = newUid;

  Users.push(user);

  return userValidation;
};
export const updateUser = async (user: User) => {
  const userValidation = validateUser(user);

  if (userValidation.error) {
    return userValidation;
  }

  const userIndex = getUserIndex(user.uid);

  if(userIndex) {
    Users[userIndex] = user;
  }

  return userValidation;
}
export const deleteUser = async (uid: UID): Promise<UserError> => {
  const userIndex = getUserIndex(uid);

  Users.splice(userIndex, 1);
  return {
    error: false,
    message: ''
  } as UserError;
}