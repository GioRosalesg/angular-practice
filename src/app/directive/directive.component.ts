import { Component, OnInit } from '@angular/core';
import { NgIf, NgFor, NgSwitch, NgSwitchCase, NgSwitchDefault } from '@angular/common';
// Interfaces
import { DirectivePeople } from './directive';

@Component({
  selector: 'app-directive',
  standalone: true,
  imports: [
    NgIf,
    NgFor,
    NgSwitch,
    NgSwitchCase,
    NgSwitchDefault
  ],
  templateUrl: './directive.component.html',
  styleUrl: './directive.component.scss'
})
export class DirectiveComponent implements OnInit {
  loading:boolean = false;
  selectedTab: string = "";
  people: Array<DirectivePeople> = [
    {
      name: 'Jazz',
      complete: false
    },{
      name: 'Lesly',
      complete: true
    },{
      name: 'Dinorah',
      complete: false
    }, {
      name: 'Gio',
      complete: true
    }];


  ngOnInit() {
    this.loadData();
  }

  private setLoading(value: boolean) {
    this.loading = value;
  }
  
  loadData() {
    this.setLoading(true);

    setTimeout(() => {
      this.setLoading(false);
    }, 3500);
  }

  onSelectTab(selectedTab: string) {
    this.selectedTab = selectedTab;
  }
}
