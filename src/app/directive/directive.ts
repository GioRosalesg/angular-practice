export interface DirectivePeople {
  name: string;
  complete: boolean;
}