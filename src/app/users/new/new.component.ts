import { Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
// Libraries
import Swal from 'sweetalert2';
// Interfaces
import { User, createUser } from "../../models/users";

@Component({
  selector: 'app-new',
  standalone: true,
  imports: [
    RouterLink,
    FormsModule
  ],
  templateUrl: './new.component.html',
  styleUrl: './new.component.scss'
})
export class NewComponent {
  user: User = {
    uid: '',
    firstName: '',
    lastName: '',
  };

  constructor(private router: Router) {}

  onSubmit() {
    const {error, message} = createUser(this.user);

    if(error) {
      Swal.fire({
        title: 'User not created',
        text: message,
        icon: 'error',
        confirmButtonText: 'Understood!'
      })
      return;
    }

    Swal.fire({
      title: 'User created!',
      text: 'User created successfully, redirecting to users page...',
      icon: 'success',
      confirmButtonText: 'Yeah!'
    });

    setTimeout(() => {
      this.router.navigate(['users']);
    }, 2000);

    return;
  }
    
}
