import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { NgFor } from '@angular/common';
import Swal from 'sweetalert2';
// Model
import { User, Users, UID, deleteUser } from "../../models/users";

@Component({
  selector: 'app-index',
  standalone: true,
  imports: [
    RouterLink,
    NgFor
  ],
  templateUrl: './index.component.html',
  styleUrl: './index.component.scss'
})
export class IndexComponent {
  users: User[] = Users;

  onDelete(userId: UID) {
    // TODO: insert user deletion functionality
    Swal.fire({
      title: 'Are you sure?',
      text: 'This action cannot be undo.',
      icon: 'warning',
      confirmButtonText: 'Yes!, delete it',
      cancelButtonText: 'No, keep it'
    }).then(async (result) => {
      if(result.isConfirmed) {
        const {error} = await deleteUser(userId);

        if(error) {
          return;
        } 
        
        Swal.fire("Deleted successfully!", "", 'success');
      }
    })
  }
}
