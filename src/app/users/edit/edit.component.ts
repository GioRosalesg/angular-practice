import { Component, OnInit } from '@angular/core';
import { NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
// Model
import { User, fetchUser, updateUser } from '../../models/users';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [
    RouterLink,
    NgIf,
    FormsModule
  ],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.scss'
})
export class EditComponent implements OnInit {
  user: User = {
    firstName: '',
    lastName: '',
    uid: ''
  };
  isFetchingUser: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router){}

  async ngOnInit(){
    this.isFetchingUser = true;
    const user = await fetchUser(this.route.snapshot.params["id"]) as User;

    if(!user) {
      this.router.navigate(['users']);
    }

    this.user = user;
    this.isFetchingUser = false;
  }

  async onUpdate() {
    const {error, message} = await updateUser(this.user);

    if(error) {
      Swal.fire({
        title: `${this.user.firstName} not updated`,
        text: message,
        icon: 'error',
        confirmButtonText: 'Understood!'
      });
      return;
    }

    Swal.fire({
      title: 'Success',
      text: `${this.user.firstName} updated successfully, redirecting to users...`,
      icon: 'success',
      confirmButtonText: 'Yeah!'
    });

    setTimeout(() => {
      this.router.navigate(['users']);
    }, 2000);
  }
}
