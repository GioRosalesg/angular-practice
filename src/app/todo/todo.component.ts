import { Component } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
// interfaces
import { ToDo, ToDos, MIN_TODO_LENGTH, SelectedTab, Hash, ToDoValidation } from './todo';

@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [
    NgFor,
    NgIf,
    FormsModule
  ],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.scss'
})
export class TodoComponent {
  todo: ToDo = {
    description: "",
    completed: false,
  };
  todos: ToDos = {};
  error: string = "";
  selectedTab: SelectedTab = "ALL";
  todosDisplayed: [Hash, ToDo][] = [];

  addToDo() {
    const toDoValidation = this.validateToDo(this.todo.description);

    if(toDoValidation.error) {
      this.error = toDoValidation.message!;
      return;
    }

    this.todos[toDoValidation.hash!] = this.todo;
    this.error = "";
    this.resetToDo();
    this.updateToDosDisplayed();
  }
  resetToDo() {
    this.todo = {
      description: "",
      completed: false,
    };
    
    return;
  }
  validateToDo(toDoDescription: string): ToDoValidation {
    const hash = this.cyrb53(toDoDescription);

    if(this.todos[hash]) {
      return {
        error: true,
        message: "Current ToDo already exists"
      };
    }
    if (toDoDescription.length < MIN_TODO_LENGTH) {
      return {
        error: true,
        message: `ToDo description must contain at least ${MIN_TODO_LENGTH} characters.`
      };
    }

    return {
      error: false,
      hash
    };
  }
  toggleCompleteToDo(todoId: Hash) {
    this.todos[todoId].completed = !this.todos[todoId].completed;

    this.updateToDosDisplayed();
  }
  handleChangeTab(selectedTab: SelectedTab) {
    this.selectedTab = selectedTab;

    this.updateToDosDisplayed();
  }
  updateToDosDisplayed() {
    switch(this.selectedTab) {
      case "COMPLETED":
        this.todosDisplayed = Object.entries(this.todos)
          .filter(([_, toDo]) => toDo.completed);
        break;
      case "INCOMPLETE":
        this.todosDisplayed = Object.entries(this.todos)
          .filter(([_, toDo]) => !toDo.completed);
        break;
      default:
        this.todosDisplayed = Object.entries(this.todos);
    }
  }
  private cyrb53(str: string, seed: number = 0): Hash {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
      ch = str.charCodeAt(i);
      h1 = Math.imul(h1 ^ ch, 2654435761);
      h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507);
    h1 ^= Math.imul(h2 ^ (h2 >>> 13), 3266489909);
    h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507);
    h2 ^= Math.imul(h1 ^ (h1 >>> 13), 3266489909);

    return (4294967296 * (2097151 & h2) + (h1 >>> 0)).toString();
  };
}
