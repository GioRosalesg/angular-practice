export type Hash = string;
export type SelectedTab = "ALL" | "COMPLETED" | "INCOMPLETE";

export interface ToDo {
  description: string;
  completed: boolean;
}
export interface ToDos {
  [key: Hash]: ToDo;
}
export interface ToDoValidation {
  error: boolean;
  message?: string;
  hash?: Hash;
}

export const MIN_TODO_LENGTH: number = 4;